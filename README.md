*Custom configuration management for single site, Multi site & Multilingual.*

This module is more help full where you want to save multiple configuration
like Facebook, Twitter, Google Auth key and etc.

This module also work with multi domain, multi site and multilingual.

***This module provides the following functionality:***
1. Create un-limited number of configuration.
2. Easy to use and understand.
3. Access saved configuration by machine name.
4. Set configuration as Active or Inactive, incase of inactive if you try to 
access configuration it will return null.
5. Provides service to access configuration value by machine name.
6. If you are trying to access any non-exists machine name value, 
it will return null.
