<?php

namespace Drupal\custom_configuration\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\custom_configuration\Helper\ConfigurationHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class to Delete Configuration Form.
 *
 * @package Drupal\custom_configuration\Form
 */
class DeleteConfigurationForm extends ConfigFormBase {

  /**
   * Database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Config helper service object.
   *
   * @var object
   */
  protected $configHelper;

  /**
   * Construction of the Delete Configuration.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Connection Class.
   * @param \Drupal\custom_configuration\Helper\ConfigurationHelper $helper
   *   ConfigurationHelper class.
   */
  public function __construct(Connection $connection, ConfigurationHelper $helper) {
    $this->database = $connection;
    $this->configHelper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'), $container->get('custom.configuration')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'custom_configuration.delete_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_configuration_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $configId = $this->getRouteMatch()->getParameter('custom_config_id');
    if ($configId) {
      $resultSet = $this->database->select('custom_configuration', 'cc')
        ->fields('cc',
        ['custom_config_name'])
        ->condition('custom_config_id', $configId)
        ->execute()->fetchObject();
      if (!empty($resultSet)) {
        $form['helptext'] = [
          '#type' => 'item',
          '#markup' => $this->t('Are you sure you want to delete the <b>@label</b> configuration ?', [
            '@label' => $resultSet->custom_config_name,
          ]),
        ];
        $form['delete'] = [
          '#type' => 'submit',
          '#value' => $this->t('Delete Configuration'),
        ];
      }
      else {
        $this->messenger()->addError('Configuration does not exists.');
      }
      $form['cancel'] = [
        '#type' => 'submit',
        '#value' => $this->t('Cancel'),
      ];
    }
    return $form;
  }

  /**
   * Submission of the delete form.
   *
   * @param array $form
   *   Array of Form data.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $configId = $this->getRouteMatch()->getParameter('custom_config_id');
    if ($form_state->getValue('op')->getUntranslatedString() === "Delete Configuration") {
      $result = $this->configHelper->deleteValue($configId);
      $this->messenger()->addMessage($result['message'], $result['status']);
    }
    $form_state->setRedirectUrl(Url::fromRoute('custom_configuration.configuration_list'));
  }

}
